# Meetical code challenge
The task is [here](https://bitbucket.org/lugott/meetical-code-challenge/src/main/code-challenge.md).

Time spent: **8 hours**

### Acceptance criteria 
1. **DONE** List all meetings with summary, start date (e.g. "May 5, 2021"), start time (e.g. "16:00"), number of attendees
2. ~~Allow storing meeting notes as text for any of the events~~
3. ~~Allow to configure a time zone (an application property would be sufficient) and display date and time in the configured time zone~~
4. ~~Show min and max start date, and total count of the entire collection of meetings~~
5. **DONE** Order meetings by start datetime

### Bonus points
1. ~~Highlight today's meetings~~
2. **DONE** Allow to filter the list of events by attendee email. For example via an input field or dropdown.
3. ~~Show a list of all attendees, and a count for each to show in how many meetings they appear~~
4. **DONE** Only show events with 1 or more attendees
5. ~~Group recurring events~~
6. ~~Specify any additional features you want as acceptance criteria~~
7. ~~Implement the additional features you specified :)~~

### REST API
#### Switching users
+ Works by changing username in URL
  
  */anna/events* or */john/events*
#### Sorting
+ Response with already sorted (by start datetime)
#### Filtering
+ By minimum attendees. Supports 0(all), 1(with attendees) or more.
+ By attendee's email. Supports one or more emails (comma separated).

*/john/events?filterAttendees=falco@meetical.onmicrosoft.com,lukas@meetical.onmicrosoft.com&minAttendees=3*

#### Format
+ All data represented in "Meetical API" format ([meetical-property.json](https://bitbucket.org/lugott/meetical-code-challenge/src/main/resources/meetical-property.json))

Example:
```
// http://localhost:8080/john/events?filterAttendees=falco@meetical.onmicrosoft.com,lukas@meetical.onmicrosoft.com&minAttendees=3

[
  {
    "isMeeticalPage": 1,
    "event": {
      "id": "AAMkADZiNDg2YmFhLTE1ODYtNGU3ZS05YjQ2LTUzYzA5Nzk1YjYxYQBGAAAAAAA_L5cqTQMaT52SorIgKc3mBwDQucIUDcUfRrPlZHtlassfAAAAAAENAADQucIUDcUfRrPlZHtlassfAAAgBz4GAAA=",
      "isCancelled": 0,
      "recurringEventId": null,
      "summary": "Nice meeting",
      "description": "",
      "location": null,
      "startDateTime": "2021-04-21T14:30:00.000+00:00",
      "endDateTime": "2021-04-21T15:00:00.000+00:00",
      "recurringRulesHumanFriendly": null,
      "recurringRulesRaw": null,
      "recurringEndDate": null,
      "calendarHtmlLinkProxy": null,
      "attendees": [
        {
          "email": "lukas@meetical.onmicrosoft.com",
          "isOrganizer": 1,
          "isResource": 0,
          "responseStatus": "none"
        },
        {
          "email": "falco@meetical.onmicrosoft.com",
          "isOrganizer": 0,
          "isResource": 0,
          "responseStatus": "none"
        },
        {
          "email": "AdeleV@meetical.onmicrosoft.com",
          "isOrganizer": 0,
          "isResource": 0,
          "responseStatus": "none"
        }
      ]
    },
    "calendarId": null,
    "templateId": -2,
    "subPageTemplateId": null,
    "meetingPageType": null,
    "calendarProvider": "MICROSOFT",
    "recurringAutomation": null
  }
]
```