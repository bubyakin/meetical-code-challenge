package io.meetical.challenge.domain;

public enum CalendarProvider {
    GOOGLE,
    MICROSOFT;
}
