package io.meetical.challenge.domain.google;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class GoogleEvent {
    private List<Item> items;
    private String summary;

/*  Fields that not in use
    private String kind;
    private String etag;
    private Date updated;
    private String timeZone;
    private String accessRole;
    private DefaultReminder[] defaultReminders;
    private String nextPageToken;
*/
}
