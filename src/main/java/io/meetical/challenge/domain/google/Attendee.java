package io.meetical.challenge.domain.google;

import lombok.Data;

@Data
public class Attendee {
    private String email;
    private boolean self;
    private boolean organizer;
    private String displayName;
    private String responseStatus;
}
