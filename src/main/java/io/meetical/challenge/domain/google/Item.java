package io.meetical.challenge.domain.google;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class Item {
    private String id;
    private String summary;
    private String status;
    private String description;
    private Start start;
    private Start end;
    private List<Attendee> attendees = new LinkedList<>();
    private Organizer organizer;

/* Fields that not in use
    private String[] recurrence;
    private String iCalUID;
    private int sequence;
    private ExtendedProperties extendedProperties;
    private String eventType;
    private Creator creator;
    private String kind;
    private String etag;
    private String htmlLink;
    private Date created;
    private Date updated;
 */
}
