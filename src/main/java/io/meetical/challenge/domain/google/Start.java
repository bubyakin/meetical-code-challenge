package io.meetical.challenge.domain.google;

import lombok.Data;

import java.util.Date;

@Data
public class Start {
    private Date dateTime;
}
