package io.meetical.challenge.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(of = {"username"})
public class User {
    private String username;
    private CalendarProvider provider;
    private String calendarUrl;
}
