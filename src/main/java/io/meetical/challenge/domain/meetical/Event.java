package io.meetical.challenge.domain.meetical;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class Event {
    private String id;
    private int isCancelled;
    private String recurringEventId;
    private String summary;
    private String description;
    private String location;
    private Date startDateTime;
    private Date endDateTime;
    private String recurringRulesHumanFriendly;
    private String recurringRulesRaw;
    private String recurringEndDate;
    private String calendarHtmlLinkProxy;
    private List<Attendee> attendees;
}
