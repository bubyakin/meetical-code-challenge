package io.meetical.challenge.domain.meetical;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Attendee {
    private String email;
    private int isOrganizer;
    private int isResource;
    private String responseStatus;
}
