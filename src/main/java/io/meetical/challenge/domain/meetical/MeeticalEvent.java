package io.meetical.challenge.domain.meetical;

import io.meetical.challenge.domain.CalendarProvider;
import lombok.Builder;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Data
@Builder
public class MeeticalEvent implements Comparable<MeeticalEvent> {
    @Builder.Default
    private int isMeeticalPage = 1;
    private Event event;
    private String calendarId;
    @Builder.Default
    private int templateId = -2;
    private String subPageTemplateId;
    private String meetingPageType;
    private CalendarProvider calendarProvider;
    private String recurringAutomation;

    public static Predicate<MeeticalEvent> hasMoreAttendeesThan(int count) {
        return e -> e.getEvent().getAttendees().size() >= count;
    }

    public static Predicate<MeeticalEvent> hasOneOfAttendees(List<String> emails) {
        return e ->
                CollectionUtils.isEmpty(emails) ||
                        CollectionUtils.containsAny(
                                e.getEvent().getAttendees().stream()
                                        .map(Attendee::getEmail)
                                        .collect(Collectors.toList()),
                                emails);
    }

    @Override
    public int compareTo(MeeticalEvent o) {
        return event.getStartDateTime().compareTo(o.getEvent().getStartDateTime());
    }
}
