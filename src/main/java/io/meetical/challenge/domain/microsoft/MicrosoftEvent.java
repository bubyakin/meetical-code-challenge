package io.meetical.challenge.domain.microsoft;

import lombok.Data;

import java.util.List;

@Data
public class MicrosoftEvent {
    private String odatacontext;
    private List<Value> value;
}
