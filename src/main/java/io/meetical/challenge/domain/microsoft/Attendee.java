package io.meetical.challenge.domain.microsoft;

import lombok.Data;

@Data
public class Attendee {
    private String type;
    private ResponseStatus status;
    private EmailAddress emailAddress;
}
