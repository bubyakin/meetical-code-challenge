package io.meetical.challenge.domain.microsoft;

import lombok.Data;

import java.util.List;

@Data
public class Value {
    private String id;
    private String subject;
    private String bodyPreview;
    private boolean isCancelled;
    private Start start;
    private Start end;
    private List<Attendee> attendees;
    private Organizer organizer;

/*  Fields that not in use
    private String odataetag;
    private Date createdDateTime;
    private Date lastModifiedDateTime;
    private String changeKey;
    private String[] categories;
    private String transactionId;
    private String originalStartTimeZone;
    private String originalEndTimeZone;
    private String iCalUId;
    private int reminderMinutesBeforeStart;
    private boolean isReminderOn;
    private boolean hasAttachments;
    private String importance;
    private String sensitivity;
    private boolean isAllDay;
    private boolean responseRequested;
    private String seriesMasterId;
    private String showAs;
    private String type;
    private String webLink;
    private String onlineMeetingUrl;
    private boolean isOnlineMeeting;
    private String onlineMeetingProvider;
    private boolean allowNewTimeProposals;
    private boolean isDraft;
    private boolean hideAttendees;
    private Body body;
    private String onlineMeeting;
    private ResponseStatus responseStatus;
    private Location location;
    private String[] locations;
    private Recurrence recurrence;
*/
}
