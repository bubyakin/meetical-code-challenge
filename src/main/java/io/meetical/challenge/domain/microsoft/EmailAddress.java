package io.meetical.challenge.domain.microsoft;

import lombok.Data;

@Data
public class EmailAddress {
    private String name;
    private String address;
}
