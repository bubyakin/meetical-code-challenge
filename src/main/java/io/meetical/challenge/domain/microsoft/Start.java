package io.meetical.challenge.domain.microsoft;

import lombok.Data;

import java.util.Date;

@Data
public class Start {
    private Date dateTime;
    private String timeZone;
}
