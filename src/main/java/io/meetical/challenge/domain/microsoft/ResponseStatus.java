package io.meetical.challenge.domain.microsoft;

import lombok.Data;

import java.util.Date;

@Data
public class ResponseStatus {
    private String response;
    private Date time;
}
