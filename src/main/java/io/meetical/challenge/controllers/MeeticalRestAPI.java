package io.meetical.challenge.controllers;

import io.meetical.challenge.UserHolder;
import io.meetical.challenge.domain.User;
import io.meetical.challenge.domain.google.GoogleEvent;
import io.meetical.challenge.domain.meetical.MeeticalEvent;
import io.meetical.challenge.domain.microsoft.MicrosoftEvent;
import io.meetical.challenge.integration.google.GoogleMeeticalEventConvertor;
import io.meetical.challenge.integration.google.GoogleService;
import io.meetical.challenge.integration.microsoft.MicrosoftMeeticalEventConvertor;
import io.meetical.challenge.integration.microsoft.MicrosoftService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static io.meetical.challenge.domain.CalendarProvider.GOOGLE;
import static io.meetical.challenge.domain.CalendarProvider.MICROSOFT;
import static io.meetical.challenge.domain.meetical.MeeticalEvent.hasMoreAttendeesThan;
import static io.meetical.challenge.domain.meetical.MeeticalEvent.hasOneOfAttendees;

@RestController
public class MeeticalRestAPI {

    private final UserHolder userHolder = new UserHolder();
    private final MicrosoftService microsoftService = new MicrosoftService();
    private final GoogleService googleService = new GoogleService();

    @GetMapping("{username}/events")
    public List<MeeticalEvent> getEvents(@PathVariable String username,
                                         @RequestParam(required = false, defaultValue = "0") Integer minAttendees,
                                         @RequestParam(required = false) List<String> filterAttendees) {
        User user = userHolder.getUser(username);
        List<MeeticalEvent> result = Collections.emptyList();
        if (user.getProvider().equals(GOOGLE)) {
            result = getFromGoogle(user.getCalendarUrl());
        } else if (user.getProvider().equals(MICROSOFT)) {
            result = getFromMicrosoft(user.getCalendarUrl());
        }

        return result.stream()
                .filter(hasMoreAttendeesThan(minAttendees))
                .filter(hasOneOfAttendees(filterAttendees))
                .sorted()
                .collect(Collectors.toList());
    }

    private List<MeeticalEvent> getFromGoogle(String calendarUrl) {
        GoogleEvent events = googleService.retrieveUserEvents(calendarUrl);
        return GoogleMeeticalEventConvertor.convert(events);
    }

    private List<MeeticalEvent> getFromMicrosoft(String calendarUrl) {
        MicrosoftEvent events = microsoftService.retrieveUserEvents(calendarUrl);
        return MicrosoftMeeticalEventConvertor.convert(events);
    }

}
