package io.meetical.challenge.integration.google;

import io.meetical.challenge.domain.CalendarProvider;
import io.meetical.challenge.domain.google.GoogleEvent;
import io.meetical.challenge.domain.google.Item;
import io.meetical.challenge.domain.google.Organizer;
import io.meetical.challenge.domain.meetical.Attendee;
import io.meetical.challenge.domain.meetical.Event;
import io.meetical.challenge.domain.meetical.MeeticalEvent;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class GoogleMeeticalEventConvertor {

    private GoogleMeeticalEventConvertor() {
    }

    public static List<MeeticalEvent> convert(GoogleEvent event) {
        List<MeeticalEvent> meeticalEvents = new LinkedList<>();
        for (Item item :
                event.getItems()) {
            meeticalEvents.add(convertItem(event.getSummary(), item));
        }
        return meeticalEvents;
    }

    private static MeeticalEvent convertItem(String summary, Item item) {
        int isCancelled = "cancelled".equals(item.getStatus()) ? 1 : 0;
        List<Attendee> attendees = convertAttendees(item.getOrganizer(), item.getAttendees());
        Date start = item.getStart() != null ? item.getStart().getDateTime() : null;
        Date end = item.getEnd() != null? item.getEnd().getDateTime(): null;
        return MeeticalEvent.builder()
                .calendarId(summary)
                .calendarProvider(CalendarProvider.GOOGLE)
                .event(Event.builder()
                        .id(item.getId())
                        .isCancelled(isCancelled)
                        .summary(item.getSummary())
                        .description(item.getDescription())
                        .startDateTime(start)
                        .endDateTime(end)
                        .attendees(attendees)
                        .build())
                .build();
    }

    private static List<Attendee> convertAttendees(Organizer organizer, List<io.meetical.challenge.domain.google.Attendee> attendees) {
        return attendees.stream()
                .map(googleAttendee -> Attendee.builder()
                        .email(googleAttendee.getEmail())
                        .isOrganizer(checkOrganizer(organizer, googleAttendee))
                        .responseStatus(googleAttendee.getResponseStatus())
                        .build()
                ).collect(Collectors.toList());
    }

    private static int checkOrganizer(Organizer organizer, io.meetical.challenge.domain.google.Attendee microsoftAttendee) {
        String organizerEmail = organizer.getEmail();
        String candidateEmail = microsoftAttendee.getEmail();
        if (Objects.equals(organizerEmail, candidateEmail)) {
            return 1;
        }
        return 0;
    }

}
