package io.meetical.challenge.integration.google;

import io.meetical.challenge.domain.google.GoogleEvent;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

public class GoogleService {

    private static final String DEFAULT_CALENDAR_URL = "https://bitbucket.org/lugott/meetical-code-challenge/raw/47353009dfbf28f8371cf019e6c40a9f044f5089/resources/google-event-data-anna-berlin.json";
    private final RestTemplate restTemplate = new RestTemplate();

    public GoogleService() {
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
    }

    public GoogleEvent retrieveUserEvents() {
        return retrieveUserEvents(DEFAULT_CALENDAR_URL);
    }

    public GoogleEvent retrieveUserEvents(String url) {
        ResponseEntity<GoogleEvent> response = restTemplate.getForEntity(url, GoogleEvent.class);
        return response.getBody();
    }
}
