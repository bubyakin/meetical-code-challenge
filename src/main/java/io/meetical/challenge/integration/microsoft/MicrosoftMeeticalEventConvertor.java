package io.meetical.challenge.integration.microsoft;

import io.meetical.challenge.domain.CalendarProvider;
import io.meetical.challenge.domain.meetical.Attendee;
import io.meetical.challenge.domain.meetical.Event;
import io.meetical.challenge.domain.meetical.MeeticalEvent;
import io.meetical.challenge.domain.microsoft.EmailAddress;
import io.meetical.challenge.domain.microsoft.MicrosoftEvent;
import io.meetical.challenge.domain.microsoft.Organizer;
import io.meetical.challenge.domain.microsoft.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MicrosoftMeeticalEventConvertor {

    private MicrosoftMeeticalEventConvertor() {
    }

    public static List<MeeticalEvent> convert(MicrosoftEvent event) {
        List<MeeticalEvent> meeticalEvents = new LinkedList<>();
        String calendarId = StringUtils.substringBetween(event.getOdatacontext(), "('", "')");
        for (Value value :
                event.getValue()) {
            meeticalEvents.add(convertValue(calendarId, value));
        }
        return meeticalEvents;
    }

    private static MeeticalEvent convertValue(String calendarId, Value value) {
        int isCancelled = value.isCancelled() ? 1 : 0;
        List<Attendee> attendees = convertAttendees(value.getOrganizer(), value.getAttendees());
        Date start = value.getStart() != null ? value.getStart().getDateTime() : null;
        Date end = value.getEnd() != null ? value.getEnd().getDateTime() : null;
        return MeeticalEvent.builder()
                .calendarId(calendarId)
                .calendarProvider(CalendarProvider.MICROSOFT)
                .event(Event.builder()
                        .id(value.getId())
                        .isCancelled(isCancelled)
                        .summary(value.getSubject())
                        .description(value.getBodyPreview())
                        .startDateTime(start)
                        .endDateTime(end)
                        .attendees(attendees)
                        .build())
                .build();
    }

    private static List<Attendee> convertAttendees(Organizer organizer, List<io.meetical.challenge.domain.microsoft.Attendee> attendees) {
        return attendees.stream()
                .map(microsoftAttendee -> Attendee.builder()
                        .email(microsoftAttendee.getEmailAddress().getAddress())
                        .isOrganizer(checkOrganizer(organizer, microsoftAttendee))
                        .responseStatus(microsoftAttendee.getStatus().getResponse())
                        .build()
                ).collect(Collectors.toList());
    }

    private static int checkOrganizer(Organizer organizer, io.meetical.challenge.domain.microsoft.Attendee microsoftAttendee) {
        EmailAddress organizerEmail = organizer.getEmailAddress();
        EmailAddress candidateEmail = microsoftAttendee.getEmailAddress();
        if ((organizerEmail != null && candidateEmail != null) &&
                Objects.equals(candidateEmail.getAddress(), organizerEmail.getAddress())) {
            return 1;
        }
        return 0;
    }
}
