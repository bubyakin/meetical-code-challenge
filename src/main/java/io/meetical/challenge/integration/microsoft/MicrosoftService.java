package io.meetical.challenge.integration.microsoft;

import io.meetical.challenge.domain.microsoft.MicrosoftEvent;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

public class MicrosoftService {

    private final static String DEFAULT_CALENDAR_URL = "https://bitbucket.org/lugott/meetical-code-challenge/raw/47353009dfbf28f8371cf019e6c40a9f044f5089/resources/microsoft-event-data.json";
    private final RestTemplate restTemplate = new RestTemplate();

    public MicrosoftService() {
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN));
        restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
    }

    public MicrosoftEvent retrieveUserEvents() {
        return retrieveUserEvents(DEFAULT_CALENDAR_URL);
    }

    public MicrosoftEvent retrieveUserEvents(String url) {
        ResponseEntity<MicrosoftEvent> response = restTemplate.getForEntity(url, MicrosoftEvent.class);
        return response.getBody();
    }

}
