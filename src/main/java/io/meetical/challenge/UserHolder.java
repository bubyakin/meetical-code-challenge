package io.meetical.challenge;

import io.meetical.challenge.domain.CalendarProvider;
import io.meetical.challenge.domain.User;

import java.util.*;

public class UserHolder {

    private Map<String, User> users = new HashMap<>();

    public UserHolder() {
        User anna = User.builder()
                .username("Anna")
                .provider(CalendarProvider.GOOGLE)
                .calendarUrl("https://bitbucket.org/lugott/meetical-code-challenge/raw/47353009dfbf28f8371cf019e6c40a9f044f5089/resources/google-event-data-anna-berlin.json")
                .build();
        users.put(anna.getUsername().toLowerCase(), anna);

        User john = User.builder()
                .username("John")
                .provider(CalendarProvider.MICROSOFT)
                .calendarUrl("https://bitbucket.org/lugott/meetical-code-challenge/raw/47353009dfbf28f8371cf019e6c40a9f044f5089/resources/microsoft-event-data.json")
                .build();
        users.put(john.getUsername().toLowerCase(), john);
    }

    public User getUser(String username) {
        return users.get(username.toLowerCase());
    }
}
