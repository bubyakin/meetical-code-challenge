package io.meetical.challenge.integration.google;

import io.meetical.challenge.domain.google.GoogleEvent;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GoogleServiceTest {

    @Test
    void retrieveUserEvents() {
        GoogleService service = new GoogleService();
        GoogleEvent result = service.retrieveUserEvents();
        assertNotNull(result);
        assertNotNull(result.getItems());
        assertEquals(result.getItems().size(),
                237);
        assertEquals(result.getItems().get(0).getId(),
                "0ueavb460ph601q88ai4mm1fb6");
        assertEquals(result.getItems().get(1).getSummary(),
                "Series A");
        assertTrue(result.getItems().get(2).getDescription().contains("pageId=10158082"));
        assertEquals(result.getItems().get(3).getStatus(),
                "confirmed");
        assertEquals(result.getItems().get(4).getStart().getDateTime().getTime(),
                1565171100000L);
        assertEquals(result.getItems().get(236).getAttendees().size(),
                1);
    }
}