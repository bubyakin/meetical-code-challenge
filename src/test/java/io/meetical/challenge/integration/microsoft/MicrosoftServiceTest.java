package io.meetical.challenge.integration.microsoft;

import io.meetical.challenge.domain.microsoft.MicrosoftEvent;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MicrosoftServiceTest {

    @Test
    void retrieveUserEventsTest() {
        MicrosoftService service = new MicrosoftService();
        MicrosoftEvent result = service.retrieveUserEvents();
        assertNotNull(result);
        assertNotNull(result.getValue());
        assertEquals(result.getValue().size(), 12);
        assertEquals(result.getValue().get(0).getId(),
                "AAMkADZiNDg2YmFhLTE1ODYtNGU3ZS05YjQ2LTUzYzA5Nzk1YjYxYQBGAAAAAAA_L5cqTQMaT52SorIgKc3mBwDQucIUDcUfRrPlZHtlassfAAAAAAENAADQucIUDcUfRrPlZHtlassfAAAgBz4GAAA=");
        assertEquals(result.getValue().get(1).getStart().getDateTime().getTime(),
                1619010000000L);
        assertEquals(result.getValue().get(2).getEnd().getTimeZone(),
                "UTC");
        assertEquals(result.getValue().get(3).getAttendees().size(),
                2);
        assertTrue(result.getValue().get(11).getBodyPreview().contains("pageId=356614145"));
    }
}